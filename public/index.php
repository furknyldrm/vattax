<?php

/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylor@laravel.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels great to relax.
|
*/

require __DIR__.'/../bootstrap/autoload.php';

/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can handle the incoming request
| through the kernel, and send the associated response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/

    if( $_SERVER['REQUEST_METHOD']=='POST' && !empty( $_POST['task'] ) && $_POST['task']=='check' ){
        ob_clean();

        $result=null;

        function curl( $url=NULL, $options=NULL, $headers=false ){

            $curl=curl_init();

         
            curl_setopt( $curl, CURLOPT_URL,trim( $url ) );
            curl_setopt( $curl, CURLOPT_AUTOREFERER, true );
            curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
            curl_setopt( $curl, CURLOPT_FAILONERROR, true );
            curl_setopt( $curl, CURLOPT_HEADER, false );
            curl_setopt( $curl, CURLINFO_HEADER_OUT, false );
            curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $curl, CURLOPT_BINARYTRANSFER, true );
            curl_setopt( $curl, CURLOPT_CONNECTTIMEOUT, 20 );
            curl_setopt( $curl, CURLOPT_TIMEOUT, 60 );
            curl_setopt( $curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36' );
            curl_setopt( $curl, CURLOPT_MAXREDIRS, 10 );
            curl_setopt( $curl, CURLOPT_ENCODING, '' );

            if( isset( $options ) && is_array( $options ) ){
                foreach( $options as $param => $value ) curl_setopt( $curl, $param, $value );
            }

            if( $headers && is_array( $headers ) ){
                curl_setopt( $curl, CURLOPT_HTTPHEADER, $headers );
            }

            $res=(object)array(
                'response'  =>  curl_exec( $curl ),
                'info'      =>  (object)curl_getinfo( $curl ),
                'errors'    =>  curl_error( $curl )
            );
            curl_close( $curl );
            return $res;
        }






        function checkvat( $code, $vatnumber, $timeout=30 ){
            $url='http://ec.europa.eu/taxation_customs/vies/services/checkVatService';

            $content = "<s11:Envelope xmlns:s11='http://schemas.xmlsoap.org/soap/envelope/'>
                <s11:Body>
                    <tns1:checkVat xmlns:tns1='urn:ec.europa.eu:taxud:vies:services:checkVat:types'>                                        
                        <tns1:countryCode>%s</tns1:countryCode>
                        <tns1:vatNumber>%s</tns1:vatNumber>
                    </tns1:checkVat>
                </s11:Body>
            </s11:Envelope>";

            $headers=array(
                'Content-Type'  =>  'text/xml; charset=utf-8',
                'SOAPAction'    =>  'checkVatService'
            );
            $options=array(
                CURLOPT_POST        =>  true,
                CURLOPT_POSTFIELDS  =>  sprintf ( $content, $code, $vatnumber )
            );
            return curl( $url, $options, $headers );
        }








        $code=$_POST['code'];
        $vatnumber=$_POST['vat'];


        $obj=checkvat( $code, $vatnumber );

        if( $obj->info->http_code==200 ){

            $dom=new DOMDocument;
            $dom->loadXML( $obj->response );

            $reqdate=$dom->getElementsByTagName('requestDate')->item(0)->nodeValue;
            $valid=$dom->getElementsByTagName('valid')->item(0)->nodeValue;
            $address=$dom->getElementsByTagName('address')->item(0)->nodeValue;

            $result=sprintf( 'VAT Number "%s" in Country-Code "%s" - Date: %s, Valid: %s, Address: %s', $vatnumber, $code, $reqdate, $valid, $address ); 
        }

        exit( $result );
    }
?>
<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta charset='utf-8' />
        <title>VAT Checker</title>
        <script>

            const ajax=function( url, params, callback ){
                let xhr=new XMLHttpRequest();
                xhr.onload=function(){
                    if( this.status==200 && this.readyState==4 )callback( this.response )
                };
                xhr.open( 'POST', url, true );
                xhr.setRequestHeader( 'Content-Type', 'application/x-www-form-urlencoded' );
                xhr.send( buildparams( params ) );
            };

            const buildparams=function(p){
                if( p && typeof( p )==='object' ){
                    p=Object.keys( p ).map(function( k ){
                        return typeof( p[ k ] )=='object' ? buildparams( p[ k ] ) : [ encodeURIComponent( k ), encodeURIComponent( p[ k ] ) ].join('=')
                    }).join('&');
                }
                return p;
            };




            document.addEventListener('DOMContentLoaded', ()=>{
                let form=document.forms.registration;
                    form.bttn.addEventListener('click', e=>{
                        let url=location.href;
                        let params={
                            'task':'check',
                            'vat':form.vat.value,
                            'code':form.code.value
                        };
                        let callback=function(r){
                            document.querySelector('pre').innerHTML=r
                        }
                        ajax.call( this, url, params, callback );
                    })
            });
        </script>
    </head>
    <body>
        <form method='post' name='registration'>
            
            <label for='vat'><input type='text' name='vat' value='10758820' /></label>
            <label for='code'><input type='text' name='code' value='GB' /></label>
            <input type='button' value='Check VAT' name='bttn' />


        </form>
        <pre></pre>
    </body>
</html>